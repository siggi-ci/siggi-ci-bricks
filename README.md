### Siggi CI Bricks

is a collection of modules needed to assemble the Siggi CI app.

#### Build

Maven-Wrapper is used to build the whole project:

```
./mvnw clean install
```
 